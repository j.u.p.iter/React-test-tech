import axios from 'axios';
import { Dispatch } from 'redux';

const ROOT_URL = 'http://reduxblog.herokuapp.com/api';
const API_KEY = '?key=8757RGD';

export const FETCH_POSTS: 'FETCH_POSTS' = 'FETCH_POSTS';
export const FETCH_POST: 'FETCH_POST' = 'FETCH_POST';
export const CREATE_POST: 'CREATE_POST' = 'CREATE_POST';
export const DELETE_POST: 'DELETE_POST' = 'DELETE_POST';

// ----- FETCH POSTS ----- //
export interface IFetchPostsAction {
  payload: any;
  type: typeof FETCH_POSTS;
};

export type FetchPostsActionCreator = () => (dispatch: Dispatch) => Promise<IFetchPostsAction>;

export const fetchPosts: FetchPostsActionCreator = () => {
	return (dispatch) => {
		return axios.get(`${ROOT_URL}/posts${API_KEY}`).then(response => (
			dispatch({
				payload: response,
				type: FETCH_POSTS,
			})
    ));
	};
};

// ----- CREATE POST ----- //
export interface ICreatePostValues {
	categories: string;
	content: string;
	title: string;
}

export interface ICreatePostAction {
  payload: any;
  type: typeof CREATE_POST;
};

export type CreatePostActionCreator = (values: ICreatePostValues, callback: () => void) =>
  (dispatch: Dispatch) => Promise<ICreatePostAction>;

export const createPost: CreatePostActionCreator = (values, callback) => {
	return (dispatch) => {
		return axios.post(`${ROOT_URL}/posts${API_KEY}`, values).then(response => {
			callback();
			return dispatch({
				payload: response,
				type: CREATE_POST,
			});
		});
	};
};

// ----- FETCH POST ----- //
export interface IFetchPostAction {
  payload: any;
  type: typeof FETCH_POST;
};

export type FetchPostActionCreator = (id: number | null) => (dispatch: Dispatch) => Promise<IFetchPostAction>;

export const fetchPost: FetchPostActionCreator = id => {
	return (dispatch) => {
		return axios.get(`${ROOT_URL}/posts/${id}${API_KEY}`).then(response => (
			dispatch({
				payload: response,
				type: FETCH_POST,
			})
		));
	};
};

// ----- DELETE POST ----- //
export interface IDeletePostAction {
  payload: any;
  type: typeof DELETE_POST;
};

export type DeletePostActionCreator = (id: number | null) => (dispatch: Dispatch) => Promise<IDeletePostAction>;

export const deletePost: DeletePostActionCreator = id => {
  return (dispatch) => {
    return axios.delete(`${ROOT_URL}/posts/${id}${API_KEY}`).then(() => (
      dispatch({
        payload: id,
        type: DELETE_POST,
      })
    ));
  };
};
