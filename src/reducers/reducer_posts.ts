import * as _ from 'lodash';
import {
  FETCH_POST,
  FETCH_POSTS,
  DELETE_POST,

  IFetchPostAction,
  IFetchPostsAction,
  IDeletePostAction,
} from '../actions';

export type PostAction = IFetchPostAction | IFetchPostsAction | IDeletePostAction;

export const postsReducer = (state = {}, action: PostAction) => {
	switch (action.type) {
		case FETCH_POST:
			return {
				...state,
				[action.payload.data.id]: action.payload.data,
			};
		case FETCH_POSTS:
			return _.mapKeys(action.payload.data, 'id');
    case DELETE_POST:
      return _.omit(state, action.payload);
		default:
			return state;
	}
};
