import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { createPost, CreatePostActionCreator } from '../../actions';
import './PostNew.css';

interface IPostNewComponentProps {
	createPost: CreatePostActionCreator;
	handleSubmit: any;
	history: any;
}

class PostNewComponent extends React.Component<IPostNewComponentProps, {}> {
	public renderField(field: any) {
		const {
			meta: { touched, error },
			input,
			label,
		} = field;
		const className = `form-group ${touched && error ? 'has-danger' : ''}`;
		return (
			<div className={className}>
				<label>{label}</label>
				<input type="text" className="form-control" {...input} />
				<div className="text-help">{touched ? error : ''}</div>
			</div>
		);
	}

	public onSubmit = (values: IValidate) => {
		this.props.createPost(values, () => {
			this.props.history.push('/');
		});
	};

	public render() {
		const { handleSubmit } = this.props;
		return (
			<form onSubmit={handleSubmit(this.onSubmit)}>
				<Field name="title" label="Post title" component={this.renderField} />
				<Field name="categories" label="Categories" component={this.renderField} />
				<Field name="content" label="Post content" component={this.renderField} />
				<button type="submit" className="btn btn-primary">
					Submit post
				</button>
				<Link to="/" className="btn btn-danger">
					Cancel
				</Link>
			</form>
		);
	}
}

interface IValidate {
	categories: string;
	content: string;
	title: string;
}
function validate(values: IValidate) {
	const errors = {
		categories: '',
		content: '',
		title: '',
	};
	if (!values.title) {
		errors.title = 'Enter a title';
	}
	if (!values.categories) {
		errors.categories = 'Enter a categorie';
	}
	if (!values.content) {
		errors.content = 'Enter a description';
	}
	return errors;
}

export const PostNew = reduxForm<any, any>({
	form: 'PostNewForm',
	validate,
})(
	connect(
		null,
		{ createPost }
	)(PostNewComponent)
);
