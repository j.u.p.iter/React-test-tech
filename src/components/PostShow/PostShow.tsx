import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  deletePost,
  fetchPost,
  ICreatePostValues,
} from '../../actions';

type PostId = number | null;

interface IPostsShowComponentProps {
	match: any;
	history: any;
	post: ICreatePostValues;
  fetchPost: (id: PostId) => any;
  deletePost: (id: PostId) => any;
}

interface IPostsShowComponentState {
	id: PostId;
}

class PostShowComponent extends React.Component<IPostsShowComponentProps, IPostsShowComponentState> {
	public state: IPostsShowComponentState = { id: null };
	constructor(props: IPostsShowComponentProps) {
		super(props);
		this.state = {
			id: this.props.match.params.id,
		};
	}
	public componentDidMount() {
		this.props.fetchPost(this.state.id);
	}
	public onDeleteClick = () => {
	  this.props.deletePost(this.state.id).then(() => this.props.history.push('/'));
	};
	public render() {
		const { post } = this.props;
		if (!post) {
			return <div>Loading...</div>;
		}

		return (
			<div>
				<Link to="/" className="btn btn-primary">
					Back
				</Link>
				<button className="btn btn-danger" onClick={this.onDeleteClick}>
					Delete this post
				</button>
				<h3>{post.title}</h3>
				<h6>Categories : {post.categories}</h6>
				<p>{this.props.post.content}</p>
			</div>
		);
	}
}

function mapStateToProps({ posts }: any, ownProps: any) {
	return { post: posts[ownProps.match.params.id] };
}

export const PostShow = connect(
	mapStateToProps,
	{
	  deletePost,
	  fetchPost,
	},
)(PostShowComponent);
